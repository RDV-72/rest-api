		Rest API: каталог библиотеки.

Ресурс - JSON.

Сущности.
1. Пользователь. Поля: id, ФИО, контакт.
2. Книга. Поля: id, название, автор, жанр.
3. Заказ. Поля: id пользователя, id книги.

Операции, поддерживаемые системой.
1. Методы GET, POST, PUT, PATСH, DELETE для каждой сущности.
2. Коды состояния 200, 201, 204, 304, 400,  401, 403, 404, 500 для каждого запроса.
3. Параметры поиска ресурсов: фильтрация, сортировка, пагинация.

Методы.
1. GET
	GET /book?genre=:genre
	GET /user?lastName=:lastName
	GET /order?user=:user&books=:books
2. PUT, POST
	POST /user
		{
		"id":'id",
		"firstName":"firstName",
		"lastName:lastName",
		"secondName:secondName",
		"address=:address"
		}
	PUT /book
		{
		"id":"id",
		"title":"title",
		"author":"author",
		"genre":"genre"
		}
3. PATСH 
	PATСH /user/:id
		{
		"address":"address"
		}
4. DELETE
	DELETE /user?id:=id
	DELETE /book?id:=id

Коды состояния.
	200 – OK – для GET
	201 – OK – для PUT и POST
	204 – OK – для DELETE
	304 – Not Modified
	400 – Bad Request
	401 – Unauthorized
	403 – Forbidden
	404 – Not found
	500 – Internal Server Error

Параметры поиска ресурсов.
1. Фильтрация
	GET /book?genre=:genre
2. Сортировка
	GET /book?sort=-genre,+title
3. Пагинация
	GET /book?sort=-genre,+title limit=10&offset=5
